import { differenceInMilliseconds } from "date-fns"

const getTimeForWork = (start, end) => {
  return differenceInMilliseconds(new Date(end), new Date(start))
}

export default getTimeForWork
