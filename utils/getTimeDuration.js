const getTimeDuration = (start, end) => {
  var diffMs = end - start
  var diffHrs = Math.floor((diffMs % 86400000) / 3600000)
  var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000)

  return `${diffHrs}ч ${diffMins}м`
}

export default getTimeDuration
