**MVP todo applications. Not all functionality is implemented**

This project created with [blitz.js](https://github.com/blitz-js/blitz).

**You’ll need to have Node 12 or newer on your local machine.**

## Start project

To get the project up and running in the browser, complete the following steps:

1. Run `npm install -g blitz` or `yarn global add blitz`. You can alternatively use npx
6. Start the development environment: run `blitz start`
7. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
