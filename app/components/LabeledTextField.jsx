import React from "react"
import { useField } from "react-final-form"

export const LabeledTextField = React.forwardRef(({ name, label, outerProps, ...props }, ref) => {
  const {
    input,
    meta: { touched, error, submitError, submitting },
  } = useField(name)

  return (
    <div {...outerProps}>
      <label>
        {label}
        <input {...input} disabled={submitting} {...props} ref={ref} />
      </label>

      {touched && (error || submitError) && (
        <div role="alert" style={{ color: "red" }}>
          {error || submitError}
        </div>
      )}
    </div>
  )
})

export default LabeledTextField
