import styles from "./Modal.module.css"

const ModalWindow = ({ children, onClose }) => (
  <div className={styles["modal-container"]}>
    <div className={styles["modal-content"]}>
      <button onClick={onClose}>X</button>
      {children}
    </div>
  </div>
)

export default ModalWindow
