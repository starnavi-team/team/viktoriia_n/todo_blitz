import { useSession, useRouter } from "blitz"
import logout from "app/auth/mutations/logout"

export default function Layout({ children, className }) {
  const session = useSession()
  const router = useRouter()
  return (
    <div>
      {session.userId && (
        <button
          title="Logout"
          className={className}
          onClick={async () => {
            router.push("/auth/login")
            await logout()
          }}
        >
          <img src="/icons/exit.png" alt="" />
        </button>
      )}
      <div>{children}</div>
    </div>
  )
}
