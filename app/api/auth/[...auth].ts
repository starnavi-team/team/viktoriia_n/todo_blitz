import { passportAuth } from "blitz"
import db from "db"
import { Strategy as GoogleStrategy } from "passport-google-oauth20"
import { format } from "date-fns"

export default passportAuth({
  successRedirectUrl: `/schedules/${format(new Date(), "yyyy-MM-dd")}`,
  errorRedirectUrl: "/404",
  strategies: [
    new GoogleStrategy(
      {
        clientID: "685362444315-i8gu95d0a8kl4cedn5ujna6l4hpncpt1.apps.googleusercontent.com",
        clientSecret: "lhVOXqUg3_L40wYi_lWkEI9w",
        callbackURL:
          process.env.NODE_ENV === "production"
            ? "https://example.com/api/auth/google/callback"
            : "http://localhost:3000/api/auth/google/callback",
        scope: ["profile", "email"],
      },
      async function (accessToken, refreshToken, profile, done) {
        const email = profile.emails && profile.emails[0]?.value

        if (!email) {
          return done(new Error("Google OAuth response doesn't have email."))
        }

        const user = await db.user.upsert({
          where: { email },
          create: {
            email,
          },
          update: { email },
        })

        const publicData = {
          userId: user.id,
          roles: [user.role],
          source: "google",
        }
        done(null, { publicData, redirectUrl: `/schedules/${format(new Date(), "yyyy-MM-dd")}` })
      }
    ),
  ],
})
