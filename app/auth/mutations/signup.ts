import db from "db"
import { AuthenticationError } from "blitz"
import { SessionContext } from "blitz"
// import {hashPassword} from "../index"
import { SignupInput } from "../validations"

export default async function signup(input, ctx: { session?: SessionContext } = {}) {
  const { email, password, password2 } = SignupInput.parse(input)

  if (password !== password2) {
    throw new AuthenticationError()
  } else {
    // const hashedPassword = await hashPassword(password)
    const user = await db.user.create({
      data: { email, hashedPassword: password, role: "user" },
      select: { id: true, email: true, role: true },
    })

    await ctx.session!.create({ userId: user.id, roles: [user.role] })

    return user
  }
}
