import { useRouter } from "blitz"
import signup from "../mutations/signup"
import styles from "../styles/SignUpForm.module.css"
import { Form, FORM_ERROR } from "app/components/Form"
import { LabeledTextField } from "app/components/LabeledTextField"
import { SignupInput } from "app/auth/validations"

const SignUpForm = () => {
  const router = useRouter()

  const onFormSubmit = async (values) => {
    try {
      await signup({ email: values.email, password: values.password, password2: values.password2 })
      router.push("/")
    } catch (error) {
      if (error.code === "P2002" && error.meta?.target?.includes("email")) {
        return { email: "This address is already in use" }
      } else {
        return {
          [FORM_ERROR]:
            "Sorry, something went wrong. Please try again.",
        }
      }
    }
  }

  return (
    <Form submitText="Зарегистрироваться" schema={SignupInput} onSubmit={onFormSubmit}>
      <LabeledTextField name="email" label="Email" />
      <LabeledTextField name="password" label="Password" type="password" />
      <LabeledTextField name="password2" label="confirm password" type="password" />
      <span className={styles["about-password-text"]}>
        The password must be at least 8 characters long. Don't use simple words and combinations.
      </span>
    </Form>
  )
}

export default SignUpForm
