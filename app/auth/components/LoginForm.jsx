import login from "../mutations/login"
import styles from "../styles/SignInForm.module.css"
import { LabeledTextField } from "app/components/LabeledTextField"
import { Form, FORM_ERROR } from "app/components/Form"
import { LoginInput } from "app/auth/validations"

const LoginForm = ({ onSuccess }) => {
  const onFormSubmit = async (values) => {
    try {
      await login({ email: values.email, password: values.password })
      onSuccess && onSuccess()
    } catch (error) {
      if (error.name === "AuthenticationError") {
        return { [FORM_ERROR]: "Sorry, these credentials are not valid" }
      } else {
        return {
          [FORM_ERROR]:
            "Sorry, something went wrong. Please try again.",
        }
      }
    }
  }

  return (
    <Form
      submitText="Войти"
      schema={LoginInput}
      initialValues={{ email: undefined, password: undefined }}
      onSubmit={onFormSubmit}
    >
      <LabeledTextField name="email" label="Email" />
      <LabeledTextField name="password" label="Password" type="password" />
      <a className={styles["sign-in-forgot-password-text"]}>Forgot password?</a>
    </Form>
  )
}

export default LoginForm
