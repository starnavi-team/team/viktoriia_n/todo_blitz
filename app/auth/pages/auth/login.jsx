import { Link } from "blitz"
import { useRouter } from "blitz"
import AuthPageTemplate from "../../../templates/AuthPageTemplate"
import commonStyles from "../../styles/AuthPage.module.css"
import LoginForm from "../../components/LoginForm"
import { format } from "date-fns"

const SignInPage = () => {
  const router = useRouter()

  return (
    <AuthPageTemplate heading="Войти">
      <LoginForm onSuccess={() => router.push(`/schedules/${format(new Date(), "yyyy-MM-dd")}`)} />
      <div className={commonStyles["auth-page-footer-text-container"]}>
        <span>Don't have an account? </span>
        <Link href="/auth/signup">
          <a>Sign up</a>
        </Link>
      </div>
    </AuthPageTemplate>
  )
}

export default SignInPage
