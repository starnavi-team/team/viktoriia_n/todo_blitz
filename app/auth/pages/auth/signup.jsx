import { Link } from "blitz"
import AuthPageTemplate from "../../../templates/AuthPageTemplate"
import commonStyles from "../../styles/AuthPage.module.css"
import SignUpForm from "../../components/SignUpForm"

const SignUpPage = () => {
  return (
    <AuthPageTemplate heading="Регистрация">
      <SignUpForm />
      <div className={commonStyles["auth-page-footer-text-container"]}>
        <span>Already have an account? </span>
        <Link href="/auth/login">
          <a>Sign in</a>
        </Link>
      </div>
    </AuthPageTemplate>
  )
}

export default SignUpPage
