/* eslint-disable jsx-a11y/no-static-element-interactions */
import styles from "../auth/styles/AuthPage.module.css"

const AuthPageTemplate = ({ heading, children }) => {
  return (
    <div className={styles["auth-page-container"]}>
      <div className={styles["auth-block-container"]}>
        <h1>{heading}</h1>
        <a href="/api/auth/google" className={styles["google-auth-button"]}>
          <img src="/icons/google-icon.png" alt="" />
          <span>Use GOOGLE</span>
        </a>
        <div className={styles["divider-container"]}>
          <hr className={styles["divider-line"]} />
          <span>or</span>
          <hr className={styles["divider-line"]} />
        </div>
        {children}
      </div>
    </div>
  )
}

export default AuthPageTemplate
