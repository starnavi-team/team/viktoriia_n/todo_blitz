import { AppProps, ErrorComponent, Head } from "blitz"
import { ErrorBoundary } from "react-error-boundary"
import { queryCache } from "react-query"
import LoginForm from "../auth/components/LoginForm"
import "../styles/globalStyles.css"
import "react-datepicker/dist/react-datepicker.css"
import "react-notifications-component/dist/theme.css"

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ErrorBoundary
      FallbackComponent={RootErrorFallback}
      onReset={() => {
        // This ensures the Blitz useQuery hooks will automatically refetch
        // data any time you reset the error boundary
        queryCache.resetErrorBoundaries()
      }}
    >
      <Head>
        <title>Timer</title>
        <link
          href="https://fonts.googleapis.com/css2?family=Raleway:wght@200;300;400;500;700&display=swap"
          rel="stylesheet"
        />
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <meta
          name="google-signin-client_id"
          content="685362444315-i8gu95d0a8kl4cedn5ujna6l4hpncpt1.apps.googleusercontent.com"
        ></meta>
      </Head>
      <Component {...pageProps} />
    </ErrorBoundary>
  )
}

function RootErrorFallback({ error, resetErrorBoundary }) {
  if (error.name === "AuthenticationError") {
    return <LoginForm onSuccess={resetErrorBoundary} />
  } else if (error.name === "AuthorizationError") {
    return (
      <ErrorComponent
        statusCode={error.statusCode}
        title="Sorry, you are not authorized to access this"
      />
    )
  } else {
    return (
      <ErrorComponent statusCode={error.statusCode || 400} title={error.message || error.name} />
    )
  }
}
