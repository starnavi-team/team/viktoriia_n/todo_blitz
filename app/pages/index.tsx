import { useSession, Link } from "blitz"
import { format } from "date-fns"

const Home = () => {
  const session = useSession()

  return (
    <div>
      <h1>Welcome!</h1>
      {session.userId ? (
        <Link href={`/schedules/${format(new Date(), "yyyy-MM-dd")}`}>
          <a>Go to schedule</a>
        </Link>
      ) : (
        <Link href="/auth/login">
          <a>Sign in</a>
        </Link>
      )}

      <style jsx>{`
        div {
          display: flex;
          height: 100vh;
          flex-direction: column;
          align-items: center;
          justify-content: center;
          background: #7700e6;
        }

        div h1 {
          color: #fff;
        }

        div a {
          width: 250px;
          height: 50px;
          background: #7700e6;
          border: 1px solid #fff;
          color: #c5c5c5;
          text-align: center;
          text-decoration: none;
          line-height: 50px;
          transition: transform 0.3s;
        }

        div a:hover {
          -webkit-box-shadow: 0px 2px 10px 0px rgba(255, 255, 255, 0.61);
          -moz-box-shadow: 0px 2px 10px 0px rgba(255, 255, 255, 0.61);
          box-shadow: 0px 2px 10px 0px rgba(255, 255, 255, 0.61);
          transform: scale(1.05);
        }
      `}</style>
    </div>
  )
}

export default Home
