import { Head, ErrorComponent, Link, useSession } from "blitz"
import { format } from "date-fns"

// ------------------------------------------------------
// This page is rendered if a route match is not found
// ------------------------------------------------------
export default function Page404() {
  const session = useSession()
  const statusCode = 404
  const title = "Этой страницы не существует"
  return (
    <>
      <Head>
        <title>
          {statusCode}: {title}
        </title>
      </Head>
      {!session.userId ? (
        <Link href="/">
          <a className="error-page-link">Home</a>
        </Link>
      ) : (
        <Link href={`/schedules/${format(new Date(), "yyyy-MM-dd")}`}>
          <a className="error-page-link">Go to schedule</a>
        </Link>
      )}
      <ErrorComponent statusCode={statusCode} title={title} />
    </>
  )
}
