import styles from "../styles/TodoListInput.module.css"
import DatePicker from "react-datepicker"
import { ru } from "date-fns/locale"
import { useState } from "react"
import createTask from "app/tasks/mutations/createTask"
import getTimeDuration from "../../../utils/getTimeDuration"
import getTimeForWork from "utils/getTimeForWork"

const TodoListInput = ({ calendarDate, setIsTaskCreated, mutate }) => {
  const [taskData, setTaskData] = useState({
    text: "",
    startTime: "",
    endTime: "",
    date: calendarDate,
  })
  const [errors, setErrors] = useState({
    text: null,
    startTime: null,
    endTime: null,
  })

  const onTimeChange = (time, name) => {
    setTaskData({ ...taskData, [name]: time })
  }

  const onInputChange = (e) => setTaskData({ ...taskData, text: e.target.value })

  const validateForm = () => {
    const newErrorsClassName = {}

    Object.keys(taskData).forEach((key) => {
      if (taskData[key] === "") {
        newErrorsClassName[key] = "invalid"
      }
    })

    setErrors(newErrorsClassName)

    const hasError = (formErrors) => Object.values(formErrors).find((e) => !!e)

    return !hasError(newErrorsClassName)
  }

  const onSubmit = async () => {
    const isValid = validateForm()

    if (isValid) {
      try {
        const res = await createTask({
          data: { ...taskData, timeForWork: getTimeForWork(taskData.startTime, taskData.endTime) },
        })
        mutate(res)
        if (res) {
          setIsTaskCreated(true)
          setTaskData({
            text: "",
            startTime: "",
            endTime: "",
            date: calendarDate,
          })
          setErrors({
            text: null,
            startTime: null,
            endTime: null,
          })
        }
      } catch (error) {
        console.log(error)
      }
    }
  }

  return (
    <>
      <div className={styles["container"]}>
        <input
          className={`${styles["todo-input"]} ${styles[errors.text]}`}
          onChange={(e) => onInputChange(e)}
          value={taskData.text}
          type="text"
          placeholder="What are you doing now?"
        />
        <span style={{ marginLeft: "50px" }}>Comment</span>
        <hr className={styles["divider"]}></hr>
        <DatePicker
          className={`${styles["timePickerInput"]} ${styles[errors.startTime]}`}
          selected={taskData.startTime}
          onChange={(time) => onTimeChange(time, "startTime")}
          showTimeSelect
          showTimeSelectOnly
          timeIntervals={5}
          timeCaption="Time"
          dateFormat="HH:mm"
          locale={ru}
        />
        <span style={{ margin: "0 10px" }}>-</span>
        <DatePicker
          className={`${styles["timePickerInput"]} ${styles[errors.endTime]}`}
          selected={taskData.endTime}
          onChange={(time) => onTimeChange(time, "endTime")}
          showTimeSelect
          showTimeSelectOnly
          timeIntervals={5}
          timeCaption="Time"
          dateFormat="HH:mm"
          locale={ru}
        />
        <hr className={styles["divider"]}></hr>
        <span>
          {taskData.startTime && taskData.endTime
            ? getTimeDuration(taskData.startTime, taskData.endTime)
            : "0h 00m"}
        </span>
        <hr className={styles["divider"]}></hr>
        <button className={styles["submit-button"]} onClick={onSubmit}>
          <span>Save</span>
          <div className={styles["check-button"]}>
            <img src="/icons/check.png" alt="" />
          </div>
        </button>
      </div>
    </>
  )
}

export default TodoListInput
