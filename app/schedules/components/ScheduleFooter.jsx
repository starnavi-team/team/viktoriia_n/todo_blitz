import styles from "../styles/ScheduleFooter.module.css"

const ScheduleFooter = () => {
  return (
    <div className={styles["container"]}>
      <button className={styles["more-button"]}>Show more days</button>
      <button className={styles["message-button"]}>
        <img src="/icons/message-square.png" alt="" />
      </button>
    </div>
  )
}

export default ScheduleFooter
