import { format } from "date-fns"
import styles from "../styles/TodoList.module.css"
import ReactTooltip from "react-tooltip"
import Timer from "react-compound-timer"
import { useState } from "react"
import { store } from "react-notifications-component"

const Todo = ({ todo, changeCompleted, onDelete, saveTimerValue }) => {
  const [isTimerStarted, setIsTimerStarted] = useState(false)
  const [intervalId, setIntervalId] = useState(null)
  const { text, startTime, endTime, id, isDone, timeForWork } = todo

  const startTimer = (startFunc, getTimeFunc, stopFunc, setCheckpoints) => {
    setIsTimerStarted(true)

    startFunc()

    setCheckpoints([
      {
        time: 500,
        callback: () => {
          console.log("stopped")
          changeCompleted(todo)
          clearInterval(intervalId)
          stopTimer(stopFunc, getTimeFunc)
        },
      },
    ])

    store.addNotification({
      message: "Timer started!",
      type: "success",
      container: "top-right",
      dismiss: {
        duration: 5000,
        onScreen: true,
      },
    })

    const intervalId = setInterval(() => saveTimerValue(id, getTimeFunc()), 5000)
    setIntervalId(intervalId)
  }

  const stopTimer = (stopFunc, getTimeFunc) => {
    clearInterval(intervalId)
    setIsTimerStarted(false)
    saveTimerValue(id, getTimeFunc())
    stopFunc()

    store.addNotification({
      message: "Timer stopped!",
      type: "info",
      container: "top-right",
      dismiss: {
        duration: 5000,
        onScreen: true,
      },
    })
  }

  const completedClass = isDone ? styles["completed-task"] : ""

  return (
    <div className={`${styles["container"]} ${completedClass}`}>
      <div className={styles["todo-task-text"]}>
        <div className={styles["round"]}>
          <input
            type="checkbox"
            id={id}
            onChange={() => {
              changeCompleted(todo)
            }}
            checked={isDone}
          />
          {/*eslint-disable-next-line jsx-a11y/label-has-associated-control */}
          <label htmlFor={id} />
        </div>
        <span>{text}</span>
        <div className={styles["todo-menu"]}>
          <button data-tip="Analitics">
            <img src="/icons/pie-chart.png" alt="" />
          </button>
          <button data-tip="Change date">
            <img src="/icons/calendar.png" alt="" />
          </button>
          <button data-tip="Priority">
            <img src="/icons/flag.png" alt="" />
          </button>
          <button data-tip="Reminder">
            <img src="/icons/bell.png" alt="" />
          </button>
          <button onClick={() => onDelete(id)} data-tip="Remove">
            <img src="/icons/archive.png" alt="" />
          </button>
          <ReactTooltip place="bottom" backgroundColor="#4f0099" />
        </div>
      </div>
      <span className={styles["comment"]}>Comment</span>
      <span style={{ marginRight: "40px" }}>
        {format(new Date(startTime), "HH:mm")} - {format(new Date(endTime), "HH:mm")}
      </span>
      {/* <span>{getTimeDuration(new Date(startTime), new Date(endTime))}</span> */}
      <Timer
        initialTime={Math.round(timeForWork)}
        direction="backward"
        lastUnit="m"
        startImmediately={false}
      >
        {({ start, stop, getTime, setCheckpoints }) => (
          <div className={isTimerStarted ? styles["timer-started"] : styles["timer"]}>
            <Timer.Hours />
            ч&nbsp;
            <Timer.Minutes />
            м&nbsp;
            <Timer.Seconds />с
            {!isTimerStarted ? (
              <button
                onClick={() => startTimer(start, getTime, stop, setCheckpoints)}
                className={styles["timer-button"]}
                disabled={isDone || timeForWork < 1000}
              >
                {isDone || timeForWork < 1000 ? (
                  <img src="/icons/play(disabled).png" alt="" />
                ) : (
                  <img src="/icons/play.png" alt="" />
                )}
              </button>
            ) : (
              <button onClick={() => stopTimer(stop, getTime)} className={styles["timer-button"]}>
                <img src="/icons/stop.png" alt="" />
              </button>
            )}
          </div>
        )}
      </Timer>
    </div>
  )
}

export default Todo
