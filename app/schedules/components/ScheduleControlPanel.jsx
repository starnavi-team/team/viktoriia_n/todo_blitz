import { useState } from "react"
import styles from "../styles/ScheduleControlPanel.module.css"
import DatePicker from "react-datepicker"
import { format, sub, add } from "date-fns"
import { useRouter } from "blitz"
import { ru } from "date-fns/locale"

const CustomInput = ({ onClick }) => (
  <button className={styles["toggle-calendar-button"]} onClick={() => onClick()}>
    <img src="/icons/calendar.png" alt="" />
    <img src="/icons/chevron-down.png" alt="" />
  </button>
)

const ScheduleControlPanel = ({ calendarDate }) => {
  const router = useRouter()

  const [isDatePickerOpen, setIsDatePickerOpen] = useState(false)
  const [startDate, setStartDate] = useState(calendarDate)

  const handleChange = (date) => {
    setStartDate(date)
    const formatedDate = format(date, "yyyy-MM-dd")
    router.push(`/schedules/${formatedDate}`)
  }

  const addDay = (date) => {
    const result = add(new Date(date), { days: 1 })
    const formatedResult = format(result, "yyyy-MM-dd")
    router.push(`/schedules/${formatedResult}`)
  }

  const substractDay = (date) => {
    const result = sub(new Date(date), { days: 1 })
    const formatedResult = format(result, "yyyy-MM-dd")
    router.push(`/schedules/${formatedResult}`)
  }

  return (
    <div className={styles["container"]}>
      <div className={styles["calendar-menu-container"]}>
        <div className={styles["calendar-container"]}>
          <button onClick={() => substractDay(calendarDate)}>
            <img src="/icons/chevron-left.png" alt="" />
          </button>
          <DatePicker
            customInputRef="forwardRef"
            selected={startDate}
            onChange={handleChange}
            locale={ru}
            customInput={
              <CustomInput
                isDatePickerOpen={isDatePickerOpen}
                setIsDatePickerOpen={setIsDatePickerOpen}
              />
            }
          />
          <button onClick={() => addDay(calendarDate)}>
            <img src="/icons/chevron-right.png" alt="" />
          </button>
        </div>
        <span>{format(calendarDate, "eeee, dd MMMM", { locale: ru })}</span>
      </div>
      <div className={styles["list-menu-container"]}>
        <div className={styles["list-menu-days-options"]}>
          <button>Day</button>
          <button>Week</button>
        </div>
        <button className={styles["plus-button"]}>
          <img src="/icons/plus.png" alt="" />
        </button>
        <button className={styles["edit-button"]}>Edit</button>
        <button className={styles["user-button"]}>
          <img src="/icons/user.png" alt="" />
          <img src="/icons/chevron-down.png" alt="" />
        </button>
      </div>
    </div>
  )
}

export default ScheduleControlPanel
