import styles from "../styles/TodoList.module.css"
import { useQuery } from "blitz"
import getTasks from "app/tasks/queries/getTasks"
import Todo from "./Todo"
import updateTask from "app/tasks/mutations/updateTask"
import TodoListInput from "./TodoListInput"
import { useState } from "react"
import SuccessModal from "../../components/Modal/SuccessModal"
import deleteTask from "app/tasks/mutations/deleteTask"

const TodoList = ({ calendarDate }) => {
  const [isTaskCreated, setIsTaskCreated] = useState(false)
  const [isDeleted, setIsDeleted] = useState(false)
  const [tasks, { mutate }] = useQuery(getTasks, { where: { date: calendarDate } })

  const changeCompleted = async (todo) => {
    try {
      const updated = await updateTask({
        where: { id: todo.id },
        data: { isDone: !todo.isDone },
      })
      mutate(updated)
    } catch (error) {
      console.log(error)
    }
  }

  const deleteTodo = async (id) => {
    try {
      const deleted = await deleteTask({
        where: { id },
      })
      mutate(deleted)
      if (deleted) {
        setIsDeleted(true)
      }
    } catch (error) {
      console.log(error)
    }
  }

  const saveTimerValue = async (id, value) => {
    try {
      const updated = await updateTask({
        where: { id },
        data: { timeForWork: value },
      })
      mutate(updated)
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div>
      <TodoListInput
        mutate={mutate}
        calendarDate={calendarDate}
        setIsTaskCreated={setIsTaskCreated}
      />

      {isTaskCreated && (
        <SuccessModal onClose={() => setIsTaskCreated(false)}>
          <h2>Task created!</h2>
          <img src="/icons/success-mark.png" alt="" />
        </SuccessModal>
      )}

      {isDeleted && (
        <SuccessModal onClose={() => setIsDeleted(false)}>
          <h2>Task deleted!</h2>
          <img src="/icons/success-mark.png" alt="" />
        </SuccessModal>
      )}

      {tasks.length > 0 ? (
        tasks.reverse().map((todo) => {
          return (
            <Todo
              onDelete={deleteTodo}
              saveTimerValue={saveTimerValue}
              changeCompleted={changeCompleted}
              key={todo.id}
              todo={todo}
            />
          )
        })
      ) : (
        <div className={styles["container"]} style={{ justifyContent: "center" }}>
          <div className={styles["no-tasks-text"]}>
            <span>You have no entries for today</span>
            <span>Add</span>
          </div>
        </div>
      )}
    </div>
  )
}

export default TodoList
