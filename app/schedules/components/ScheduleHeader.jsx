import styles from "../styles/ScheduleHeader.module.css"
import Layout from "app/layouts/Layout"
import { Link } from "blitz"

const ScheduleHeader = () => {
  return (
    <div className={styles["container"]}>
      <div className={styles["schedule-menu-container"]}>
        <Link href="/">
          <img style={{ cursor: "pointer" }} src="/icons/Timer-logo.png" alt="Logo" />
        </Link>
        <div className={styles["schedule-button"]}>
          <img src="/icons/clock.png" alt="" />
          <span>Schedule</span>
        </div>
        <div>
          <img src="/icons/pie-chart.png" alt="" />
          <span>Analytics</span>
          <button>
            <img src="/icons/chevron-down.png" alt="" />
          </button>
        </div>
      </div>
      <div className={styles["user-menu-container"]}>
        <button className={styles["user-menu-button"]}>
          <img src="/icons/settings.png" alt="" />
        </button>
        <div style={{ display: "flex" }}>
          <button className={styles["user-menu-button"]}>
            <img src="/icons/questions.png" alt="" />
          </button>
          <button className={styles["user-menu-button"]}>
            <img src="/icons/chevron-down.png" alt="" />
          </button>
        </div>
        <div className={styles["user-photo"]}></div>
        <button className={`${styles["notification-button"]} ${styles["user-menu-button"]}`}>
          <img src="/icons/bell.png" alt="" />
        </button>
        <Layout className={`${styles["exit-button"]} ${styles["user-menu-button"]}`} />
      </div>
    </div>
  )
}

export default ScheduleHeader
