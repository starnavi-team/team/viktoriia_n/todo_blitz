import React, { Suspense } from "react"
import { Head, Link, useQuery, BlitzPage } from "blitz"

export const SchedulesList = () => {
  return (
    <ul>
      <li>2324</li>
    </ul>
  )
}

const SchedulesPage: BlitzPage = () => {
  return (
    <div>
      <Head>
        <title>Schedules</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1>Schedules</h1>

        <p>
          {
            <Link href="/schedules/new">
              <a>Create Schedule</a>
            </Link>
          }
        </p>

        <Suspense fallback={<div>Loading...</div>}>
          <SchedulesList />
        </Suspense>
      </main>
    </div>
  )
}

export default SchedulesPage
