import React, { Suspense } from "react"
import { Head, BlitzPage, useParam } from "blitz"
import ScheduleHeader from "../../components/ScheduleHeader"
import ScheduleControlPanel from "../../components/ScheduleControlPanel"
import TodoList from "../../components/TodoList"
import ScheduleFooter from "../../components/ScheduleFooter"
import ReactNotification from "react-notifications-component"

export const Schedule = () => {
  return (
    <div>
      <h1>Schedule</h1>
      {/* <pre>{JSON.stringify(schedule, null, 2)}</pre>

      {
        <Link href="/schedules/[scheduleId]/edit" as={`/schedules/${schedule.id}/edit`}>
          <a>Edit</a>
        </Link>
      }

      <button
        type="button"
        onClick={async () => {
          if (window.confirm("This will be deleted")) {
            await deleteSchedule({ where: { id: schedule.id } })
            router.push("/schedules")
          }
        }}
      >
        Delete
      </button> */}
    </div>
  )
}

const ShowSchedulePage: BlitzPage = () => {
  const param = useParam("scheduleId")

  if (param) {
    return (
      <div style={{ padding: "50px 100px" }}>
        <Head>
          <title>Schedule</title>
          <link rel="icon" href="/icons/Timer-logo.png" />
        </Head>

        <main>
          <Suspense fallback={<p style={{ fontSize: "28px", margin: "40px" }}>Loading...</p>}>
            <ReactNotification />
            <ScheduleHeader />
            <ScheduleControlPanel calendarDate={new Date(param)} />
            <TodoList calendarDate={new Date(param)} />
            <ScheduleFooter />
          </Suspense>
        </main>
      </div>
    )
  } else {
    return (
      <div style={{ padding: "50px 100px" }}>
        <p style={{ fontSize: "28px", margin: "40px" }}>Loading...</p>
      </div>
    )
  }
}

export default ShowSchedulePage
