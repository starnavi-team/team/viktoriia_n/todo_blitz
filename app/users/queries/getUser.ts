import db from "db"
import { NotFoundError } from "blitz"

export default async function getUser({ where, select }, ctx: { session? } = {}) {
  ctx.session?.authorize(["admin", "user"])

  const user = await db.user.findOne({ where, select })

  if (!user) throw new NotFoundError(`User with id ${where.id} does not exist`)

  return user
}
